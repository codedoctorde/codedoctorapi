package com.gitlab.codedoctorde.api.animation;

public enum AnimationItemType {
    STRIPE,
    SINGLE,
    BLING
}
