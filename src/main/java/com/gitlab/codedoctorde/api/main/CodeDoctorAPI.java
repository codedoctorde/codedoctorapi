package com.gitlab.codedoctorde.api.main;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author CodeDoctorDE
 */
public class CodeDoctorAPI {
    private final JavaPlugin plugin;

    public CodeDoctorAPI(final JavaPlugin plugin) {
        this.plugin = plugin;
    }
}
