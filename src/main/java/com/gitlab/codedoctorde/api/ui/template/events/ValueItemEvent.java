package com.gitlab.codedoctorde.api.ui.template.events;

import org.bukkit.entity.Player;

/**
 * @author CodeDoctorDE
 */
public interface ValueItemEvent {
    boolean onEvent(float value, Player player);
}
