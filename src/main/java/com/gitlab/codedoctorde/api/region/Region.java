package com.gitlab.codedoctorde.api.region;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CodeDoctorDE
 */
public class Region {
    private List<Selection> selections = new ArrayList<>();

    public List<Selection> getSelections() {
        return selections;
    }
}
